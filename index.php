<?php

require_once('Classes/SplClassLoader.php');
$classLoader = new SplClassLoader('DTO', 'Classes');
$classLoader->register();
$classLoader = new SplClassLoader('DAO', 'Classes');
$classLoader->register();
$classLoader = new SplClassLoader('BLL', 'Classes');
$classLoader->register();

$tec = new \BLL\Tecnico(new \DAO\Tecnico);
$data = $tec->getAll();
$ntec = new \DTO\Tecnico();
$ntec->identificacion = 456789;
$ntec->nombreT = 'Test1';
$ntec->apellido1 = 'Test1_ap';
$ntec->perfil = 0;
$ntec->activo = 0;
$ntec2 = $tec->save($ntec);	
var_dump($tec, $data, $ntec2);
$ntec2->perfil = 1;
$ntec2->activo = 1;
var_dump($ntec2);
$ntec3 = $tec->update($ntec2);
var_dump($ntec3);
	