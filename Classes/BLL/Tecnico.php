<?php

namespace BLL;

Class Tecnico {
	
	private $_obj;
	
	public function __construct(\DAO\IDAO $obj) {
		$this->_obj = $obj;
	}
	
	public function getAll() {
		return $this->_obj->getAll();
	}
	
	public function get($id) {
		return $this->_obj->get($id);
	}
	
	public function save($obj) {
		return $this->_obj->save($obj);
	}
	
	public function update($obj) {
		return $this->_obj->update($obj);
	}
}