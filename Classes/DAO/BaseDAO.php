<?php

namespace DAO;

abstract class BaseDAO {
	private static $username = 'root';
	private static $password = '8453dedatos';
	private static $server = 'localhost';
	private static $port = '3306';
	private static $dbname = 'masterspa';
	
	protected final function getConexion() {
		$cnx = false;
		try {
			$dsn = 'mysql:host='.self::$server.';dbname='.self::$dbname;
			$cnx = new \PDO($dsn, self::$username, self::$password);
			$cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}
		catch (Exception $exc) {
			print($exc->getMessage());
			throw new Exception($exc->getMessage());
		}
		return $cnx;
	}
}