<?php

namespace DAO;

Class Tecnico extends BaseDAO implements IDAO {
	
	public function getAll() {
		$data = null;
		try {
			$cnx = $this->getConexion();
			$statement = $cnx->query('SELECT numera, identificacion, nombreT, apellido1, perfil, activo FROM tecnicos');
			$data = $statement->fetchAll(\PDO::FETCH_CLASS,'DTO\Tecnico');
			$statement = null;
			$cnx = null;
		}
		catch (Exception $exc) {
			throw new Exception($exc->getMessage());
		}
		return $data;
	}

	public function get($id) {
		$data = null;
		try {
			$cnx = $this->getConexion();
			$statement = $cnx->prepare('SELECT numera, identificacion, nombreT, apellido1, perfil, activo FROM tecnicos WHERE numera = ?');
			$statement->execute(array($id));
			$data = $statement->fetchAll(\PDO::FETCH_CLASS,'DTO\Tecnico');
			if (count($data) > 0)
				$data = $data[0];
			else
				$data = null;
			$statement = null;
			$cnx = null;
		}
		catch (Exception $exc) {
			throw new Exception($exc->getMessage());
		}
		return $data;
	}

	public function save($obj) {
		$data = null;
		try {
			$cnx = $this->getConexion();
			$statement = $cnx->prepare(
				'INSERT INTO tecnicos(identificacion, nombreT, apellido1, perfil, activo) 
					VALUES (:identificacion, :nombreT, :apellido1, :perfil, :activo)'
			);
			$result = $statement->execute(array(
				'identificacion' => $obj->identificacion,
				'nombreT' => $obj->nombreT,
				'apellido1' => $obj->apellido1,
				'perfil' => $obj->perfil,
				'activo' => $obj->activo
			));
			if ($result > 0)
				$data = $this->get($cnx->lastInsertId());
			$statement = null;
			$cnx = null;
		}
		catch (Exception $exc) {
			throw new Exception($exc->getMessage());
		}
		return $data;
	}
	
	public function update($obj) {
		$data = null;
		try {
			$cnx = $this->getConexion();
			$statement = $cnx->prepare(
				'UPDATE tecnicos SET
					identificacion = :identificacion, 
					nombreT = :nombreT, 
					apellido1 = :apellido1, 
					perfil = :perfil, 
					activo = :activo
				 WHERE numera = :numera'
			);
			$result = $statement->execute(array(
				'numera' => $obj->numera,
				'identificacion' => $obj->identificacion,
				'nombreT' => $obj->nombreT,
				'apellido1' => $obj->apellido1,
				'perfil' => $obj->perfil,
				'activo' => $obj->activo
			));
			if ($result > 0)
				$data = $this->get($obj->numera);
		}
		catch (Exception $exc) {
			throw new Exception($exc->getMessage());
		}
		return $data;
	}

}