<?php
namespace DAO;

interface IDAO {
	public function getAll();
	public function get($id);
	public function save($obj);
	public function update($obj);
}
